void setup() {
  Serial.begin(115200);
}

void loop() {
  int buttonIn = analogRead(A0);
  Serial.println(buttonIn);
  if (buttonIn < 50){
    Serial.println("SIDE");
  } else if (buttonIn > 330 && buttonIn < 370){
    Serial.println("RED");
  } else if (buttonIn > 510 && buttonIn < 540){
    Serial.println("GREEN");
  } else if (buttonIn > 620 && buttonIn < 640){
    Serial.println("RIGHT");
  } else if (buttonIn > 680 && buttonIn < 710){
    Serial.println("UP");
  } else if (buttonIn > 740 && buttonIn < 770){
    Serial.println("DOWN");
  } else if (buttonIn > 780 && buttonIn < 820){
    Serial.println("LEFT");
  }
  delay(50);
}
