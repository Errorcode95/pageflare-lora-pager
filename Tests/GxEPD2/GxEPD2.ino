#include <GxEPD2_BW.h>

GxEPD2_BW<GxEPD2_213_BN, GxEPD2_213_BN::HEIGHT> display(GxEPD2_213_BN(/*CS=5*/ 0, /*DC=*/ 28, /*RST=*/ 31, /*BUSY=*/ 30)); // DEPG0213BN 122x250, SSD1680

void setup()
{
  //display.init(115200); // default 10ms reset pulse, e.g. for bare panels with DESPI-C02
  display.init(115200, true, 2, false, SPI1, SPISettings(4000000, MSBFIRST, SPI_MODE0));
  
  display.setRotation(3);
  display.setTextColor(GxEPD_BLACK);
  display.setFullWindow();
  display.firstPage();
  display.fillScreen(GxEPD_WHITE);
  display.setCursor(0, 0);
  display.print("Hello World UwU");
  display.display();
  display.hibernate();
}

void loop() {};
