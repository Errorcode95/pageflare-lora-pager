//Hardware
///Display
#include <GxEPD2_BW.h>

GxEPD2_BW<GxEPD2_213_BN, GxEPD2_213_BN::HEIGHT> display(GxEPD2_213_BN(/*CS=5*/ 28, /*DC=*/ 29, /*RST=*/ 30, /*BUSY=*/ 31)); // DEPG0213BN 122x250, SSD1680

///Radio
#include <RH_RF95.h>
#include <RHEncryptedDriver.h>
#include <Speck.h>

RH_RF95 rf95;     // Instanciate a LoRa driver
Speck cipher;   // Instanciate a Speck block ciphering
RHEncryptedDriver radio(rf95, cipher); // Instantiate the driver with those two

float frequency = 915.0;
float txPower = 13;
unsigned char encryptkey[16]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}; // The very secret key 

///GNSS
#include <TinyGPS++.h>
#include <TinyGPSPlus.h>
#include <SoftwareSerial.h>

#define GPS_RX_PIN 0
#define GPS_TX_PIN 1
SoftwareSerial ss(GPS_RX_PIN, GPS_TX_PIN);

TinyGPSPlus gps;
TinyGPSTime prevTime;

///Buzzer
#define buzzerPin 40

//Software

int unreadPagesCount = 0;
int buttonPressed = 0;
String unreadPages;

String message[] = {};



void setup()
{
  ss.begin(9600);
  boot(); 
}

void loop() {
  buttons();
  powerSwitch();
  messageScan();
  //buzzer();
  gnss();
}

void buttons() {
  int buttonIn = analogRead(39);
  if (buttonIn < 50){ //Side
    buttonPressed = 1;
  } else if (buttonIn > 330 && buttonIn < 370){ //Red
    buttonPressed = 2;
  } else if (buttonIn > 510 && buttonIn < 540){ //Green
    buttonPressed = 3;
  } else if (buttonIn > 620 && buttonIn < 640){ //Right
    buttonPressed = 4;
  } else if (buttonIn > 680 && buttonIn < 710){ //Up
    buttonPressed = 5;
  } else if (buttonIn > 740 && buttonIn < 770){ //Down
    buttonPressed = 6;
  } else if (buttonIn > 780 && buttonIn < 820){ //Left
    buttonPressed = 7;
  } else {
    buttonPressed = 0;
  }
  if (!buttonPressed == 0){
    Serial.print("Button: " + buttonPressed);
  }
  delay(50);
}

void powerSwitch() {
  bool switchIn = digitalRead(15);
  bool switchState;
  if (switchIn && !switchState) {
    Serial.print("Switch ON");
  } else if (!switchIn && switchState) {
    Serial.print("Switch OFF");
  }
  switchState = switchIn;
}

void messageScan() {
  if (radio.available()) {
    // Should be a message for us now   
    uint8_t buf[radio.maxMessageLength()];
    uint8_t len = sizeof(buf);
    if (radio.recv(buf, &len)) {
      ++unreadPagesCount;
      //display.print((char *)&buf);

      char charArray[sizeof(buf)];
      for (int i = 0; i < sizeof(buf); i++) {
        charArray[i] = (char)buf[i];
      }
      String TEMPmessage(charArray);
      message[unreadPagesCount] = TEMPmessage;
      
      String newPageDisplay;
      int16_t tbx, tby; uint16_t tbw, tbh;
      display.fillScreen(GxEPD_WHITE);
      display.setTextSize(3);
      if (unreadPagesCount == 1){
        newPageDisplay = String(unreadPagesCount) + " NEW PAGE";
        display.getTextBounds(newPageDisplay, 0, 0, &tbx, &tby, &tbw, &tbh);
        uint16_t x = ((display.width() - tbw) / 2) - tbx;
        uint16_t y = ((display.height() - tbh) / 2) - tby;
        display.setCursor(x, y);
        display.print(newPageDisplay);
      } else {
        newPageDisplay = String(unreadPagesCount) + " NEW PAGES";
        display.getTextBounds(newPageDisplay, 0, 0, &tbx, &tby, &tbw, &tbh);
        uint16_t x = ((display.width() - tbw) / 2) - tbx;
        uint16_t y = ((display.height() - tbh) / 2) - tby;
        display.setCursor(x, y);
        display.print(newPageDisplay);
      }
      display.display();
      bool readlock = true;
      while (readlock){
        buttons();
        messageScan();
        if (buttonPressed == 3){
          display.fillScreen(GxEPD_WHITE);
          display.setTextSize(1);
          display.setCursor(0, 0);
          display.print("Page: ");
          display.print(unreadPagesCount);
          display.setCursor(0, 10);
          display.setTextSize(2);
          display.print(message[unreadPagesCount]);
          display.display();
          delay(150 * len);
          display.fillScreen(GxEPD_WHITE);
          display.display();
          if (unreadPagesCount == 0){
            readlock = false;
          } else {
            unreadPagesCount = unreadPagesCount - 1;
          }
        } else if (buttonPressed == 2){
          display.fillScreen(GxEPD_WHITE);
          display.display();
          readlock = false;
        }
      }
    }
  }
}

void boot() {
  display.init(115200, true, 2, false, SPI1, SPISettings(4000000, MSBFIRST, SPI_MODE0));

  display.setRotation(3);
  display.setTextColor(GxEPD_BLACK);
  display.fillScreen(GxEPD_WHITE);
  display.setFullWindow();
  display.firstPage();
  display.setCursor(0, 0);
  //PageFlare Logo
  //delay (~2000?)
  if (rf95.init()) {
    display.print("[ OK ] LoRa Initialized\n");
    display.display();
  } else {
    display.print("[FAIL] LoRa Initialization Failed\n");
    display.display();
  }
  rf95.setFrequency(frequency);
  //String frequencyDisplay = "       frequency "+ String(frequency) +"MHz";
  //display.print(frequencyDisplay);
  rf95.setTxPower(txPower);
  //String txPowerDisplay = "       TX power " + String(txPower) + "dB";
  //display.print(txPowerDisplay);
  cipher.setKey(encryptkey, 16);
  delay(2500);
  display.fillScreen(GxEPD_WHITE);
  display.display();
  /*
  display.fillScreen(GxEPD_BLACK);
  if (ss.available() > 0 && gps.encode(ss.read())) {
      display.print("[ OK ] GPS\n");
      display.display();
    }
  */
}

void buzzer() {
  
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(500);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(500);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
  delay(100);
  tone(buzzerPin, 3000);
  delay(100);
  noTone(buzzerPin);
}

void gnss(){
  
}
