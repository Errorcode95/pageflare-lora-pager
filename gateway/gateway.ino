#include <RH_RF95.h>
#include <RHEncryptedDriver.h>
#include <Speck.h>

RH_RF95 rf95;     // Instantiate a LoRa driver
Speck myCipher;   // Instantiate a Speck block ciphering
RHEncryptedDriver myDriver(rf95, myCipher); // Instantiate the driver with those two

float frequency = 915.0; // Change the frequency here.
unsigned char encryptkey[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}; // The very secret key
char receivedMessage[64]; // Buffer to store received serial data
uint8_t receivedMessageLen = 0;

void setup() {
  Serial.begin(9600);
  while (!Serial); // Wait for serial port to be available
  Serial.println("PageFlare LoRa Gateway");
  
  if (!rf95.init())
    Serial.println("LoRa init failed");
  
  // Setup ISM frequency
  rf95.setFrequency(frequency);
  // Setup Power, dBm
  rf95.setTxPower(13);
  
  myCipher.setKey(encryptkey, sizeof(encryptkey));
  
  Serial.println("Waiting for radio to setup");
  delay(1000);
  Serial.println("Setup completed");
}

void loop() {
  // Check for incoming serial data
  while (Serial.available() > 0) {
    char incomingByte = Serial.read();
    if (receivedMessageLen < sizeof(receivedMessage) - 1) {
      receivedMessage[receivedMessageLen] = incomingByte;
      receivedMessageLen++;
    }
  }

  if (receivedMessageLen > 0) {
    receivedMessage[receivedMessageLen] = '\0'; // Null-terminate the received message
    uint8_t data[receivedMessageLen + 1];
    memcpy(data, receivedMessage, receivedMessageLen + 1);
    
    myDriver.send(data, sizeof(data)); // Send received data to LoRa gateway
    Serial.print("Sent: ");
    Serial.println(receivedMessage);
    
    receivedMessageLen = 0; // Reset the received message buffer
  }
  
  delay(100); // Adjust the delay as needed
}
