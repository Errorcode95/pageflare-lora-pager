//Hardware
///Display
#include <GxEPD2_BW.h>

GxEPD2_BW<GxEPD2_213_BN, GxEPD2_213_BN::HEIGHT> display(GxEPD2_213_BN(/*CS=5*/ 0, /*DC=*/ 28, /*RST=*/ 31, /*BUSY=*/ 30)); // DEPG0213BN 122x250, SSD1680

///Radio
#include <RH_RF95.h>
#include <RHEncryptedDriver.h>
#include <Speck.h>

RH_RF95 rf95;     // Instanciate a LoRa driver
Speck cipher;   // Instanciate a Speck block ciphering
RHEncryptedDriver radio(rf95, cipher); // Instantiate the driver with those two

float frequency = 915.0;
float txPower = 13;
unsigned char encryptkey[16]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}; // The very secret key 

int unreadPages = 0;

void setup()
{
  boot(); 
}

void loop() {
  buttons();
  powerSwitch();
}

void buttons() {
  int buttonIn = analogRead(A0);
  if (buttonIn < 50){ //Side
    Serial.println("SIDE");
  } else if (buttonIn > 330 && buttonIn < 370){ //Red
    Serial.println("RED");
  } else if (buttonIn > 510 && buttonIn < 540){ //Green
    Serial.println("GREEN");
  } else if (buttonIn > 620 && buttonIn < 640){ //Right
    Serial.println("RIGHT");
  } else if (buttonIn > 680 && buttonIn < 710){ //Up
    Serial.println("UP");
  } else if (buttonIn > 740 && buttonIn < 770){ //Down
    Serial.println("DOWN");
  } else if (buttonIn > 780 && buttonIn < 820){ //Left
    Serial.println("LEFT");
  }
  delay(50);
}

void powerSwitch() {
  bool switchIn = digitalRead(15);
  bool switchState;
  if (switchIn && !switchState) {
    Serial.print("Switch ON");
  } else if (!switchIn && switchState) {
    Serial.print("Switch OFF");
  }
  switchState = switchIn;
}

void messageScan() {
  if (radio.available()) {
    // Should be a message for us now   
    uint8_t buf[radio.maxMessageLength()];
    uint8_t len = sizeof(buf);
    if (radio.recv(buf, &len)) {
      //display.print((char *)&buf);
      ++unreadPages;
      display.print(unreadPages + " New Page(s)");
      display.display();
    }
  }
}

void boot() {
  display.init(115200, true, 2, false, SPI1, SPISettings(4000000, MSBFIRST, SPI_MODE0));

  display.setRotation(3);
  display.setTextColor(GxEPD_BLACK);
  display.fillScreen(GxEPD_WHITE);
  display.setFullWindow();
  display.firstPage();
  display.setCursor(0, 0);
  //PageFlare Logo
  //delay (~2000?)
  if (rf95.init()) {
    display.print("[ OK ] LoRa\n");
    display.display();
  }
  rf95.setFrequency(frequency);
//  display.print("[ OK ] frequency "+ frequency +"MHz");
  rf95.setTxPower(txPower);
//  display.print("[ OK ] TX power "+ txPower +"dB");
  cipher.setKey(encryptkey, 16);
  delay(4000);
}
