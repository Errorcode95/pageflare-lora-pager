#include <FlexiTimer2.h>

//Hardware
///Display
#include <GxEPD2_BW.h>

GxEPD2_BW<GxEPD2_213_BN, GxEPD2_213_BN::HEIGHT> display(GxEPD2_213_BN(/*CS=5*/ 28, /*DC=*/ 29, /*RST=*/ 30, /*BUSY=*/ 31)); // DEPG0213BN 122x250, SSD1680

///Radio
#include <RH_RF95.h>
#include <RHEncryptedDriver.h>
#include <Speck.h>

RH_RF95 rf95;     // Instanciate a LoRa driver
Speck myCipher;   // Instanciate a Speck block ciphering
RHEncryptedDriver myDriver(rf95, myCipher); // Instantiate the driver with those two

float frequency = 915.0; // Change the frequency here. 
unsigned char encryptkey[16]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}; // The very secret key

///Switch
//#include "LowPower.h"
//const int wakeUpPin = 18;

///Buttons
int buttonPressed = 0;
void setup()
{
  //display.init(115200); // default 10ms reset pulse, e.g. for bare panels with DESPI-C02
  display.init(115200, true, 2, false, SPI1, SPISettings(4000000, MSBFIRST, SPI_MODE0));

  display.setRotation(3);
  display.setTextColor(GxEPD_BLACK);
  display.setFullWindow();
  display.firstPage();

  if (rf95.init())
    display.fillScreen(GxEPD_WHITE);
    display.setCursor(0, 0);
    display.print("LoRa on");
    display.display();
  // Setup ISM frequency
  rf95.setFrequency(frequency);
  // Setup Power,dBm
  rf95.setTxPower(13);
  myCipher.setKey(encryptkey, 16);
  digitalWrite(0,LOW);
  delay(4000);

//  pinMode(wakeUpPin, INPUT);
}

void loop() {
  
  if (myDriver.available()) {
    // Should be a message for us now   
    uint8_t buf[myDriver.maxMessageLength()];
    uint8_t len = sizeof(buf);
    if (myDriver.recv(buf, &len)) {
    display.print((char *)&buf);
    display.display();
    }
  }
  buttons();
  powerSwitch();
}

void buttons() {
  int buttonIn = analogRead(A15);
  if (buttonIn < 50){ //Side
    Serial.println("SIDE");
    buttonPressed = 1;
  } else if (buttonIn > 330 && buttonIn < 370){ //Red
    Serial.println("RED");
    buttonPressed = 2;
  } else if (buttonIn > 510 && buttonIn < 540){ //Green
    Serial.println("GREEN");
    buttonPressed = 3;
  } else if (buttonIn > 620 && buttonIn < 640){ //Right
    Serial.println("RIGHT");
    buttonPressed = 4;
  } else if (buttonIn > 680 && buttonIn < 710){ //Up
    Serial.println("UP");
    buttonPressed = 5;
  } else if (buttonIn > 740 && buttonIn < 770){ //Down
    Serial.println("DOWN");
    buttonPressed = 6;
  } else if (buttonIn > 780 && buttonIn < 820){ //Left
    Serial.println("LEFT");
    buttonPressed = 7;
  } else {
    buttonPressed = 0;
  }
  Serial.println(buttonPressed);
  delay(50);
}

void powerSwitch() {
  // Allow wake up pin to trigger interrupt on low.
  //attachInterrupt(0, wakeUp, LOW);
  
  // Enter power down state with ADC and BOD module disabled.
  // Wake up when wake up pin is low.
  //LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 
  
  // Disable external pin interrupt on wake up pin.
  //detachInterrupt(0); 
}

//void wakeUp()
//{
    // Just a handler for the pin interrupt.
//}
