#include <TinyGPSPlus.h>
#include <SoftwareSerial.h>

#define GPS_RX_PIN 0
#define GPS_TX_PIN 1
SoftwareSerial ss(GPS_RX_PIN, GPS_TX_PIN);

TinyGPSPlus gps;
TinyGPSTime prevTime;

//Hardware
///Display
#include <GxEPD2_BW.h>

GxEPD2_BW<GxEPD2_213_BN, GxEPD2_213_BN::HEIGHT> display(GxEPD2_213_BN(/*CS=5*/ 28, /*DC=*/ 29, /*RST=*/ 30, /*BUSY=*/ 31)); // DEPG0213BN 122x250, SSD1680

///Radio
#include <RH_RF95.h>
#include <RHEncryptedDriver.h>
#include <Speck.h>

RH_RF95 rf95;     // Instanciate a LoRa driver
Speck cipher;   // Instanciate a Speck block ciphering
RHEncryptedDriver radio(rf95, cipher); // Instantiate the driver with those two

float frequency = 915.0;
float txPower = 13;
unsigned char encryptkey[16]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}; // The very secret key 

#include <string>

void setup()
{
  ss.begin(9600);
  boot(); 
}

void loop() {
  messageScan();
  gnss();
}

void messageScan() {
  if (radio.available()) {
    // Should be a message for us now   
    uint8_t buf[radio.maxMessageLength()];
    uint8_t len = sizeof(buf);
    if (radio.recv(buf, &len)) {
      display.setCursor(0, 0);
      display.print("LoRa: " + (char *)&buf);
    }
  }
}

void gnss() {
  while (ss.available() > 0) {
    if (gps.encode(ss.read())) {
      display.setCursor(10, 0);
      display.print("GPS: " + gps.time.value());
    }
  }
}

void rtc() {
  
}

void boot() {
  display.init(115200, true, 2, false, SPI1, SPISettings(4000000, MSBFIRST, SPI_MODE0));

  display.setRotation(3);
  display.setTextColor(GxEPD_BLACK);
  display.fillScreen(GxEPD_WHITE);
  display.setFullWindow();
  display.firstPage();
  display.setCursor(0, 0);
  //PageFlare Logo
  //delay (~2000?)
  if (rf95.init()) {
    display.print("[ OK ] LoRa Initialized\n");
    display.display();
  } else {
    display.print("[FAIL] LoRa Initialization Failed\n");
    display.display();
  }
  rf95.setFrequency(frequency);
  //String frequencyDisplay = "       frequency "+ String(frequency) +"MHz";
  //display.print(frequencyDisplay);
  rf95.setTxPower(txPower);
  //String txPowerDisplay = "       TX power " + String(txPower) + "dB";
  //display.print(txPowerDisplay);
  cipher.setKey(encryptkey, 16);
  delay(2500);
  display.fillScreen(GxEPD_WHITE);
  display.display();
  /*
  display.fillScreen(GxEPD_BLACK);
  if (ss.available() > 0 && gps.encode(ss.read())) {
      display.print("[ OK ] GPS\n");
      display.display();
    }
  */
}
